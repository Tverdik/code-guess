module.exports = {
  moduleNameMapper: {
    '^@providers(.*)$': '<rootDir>/src/providers$1',
    '^@components(.*)$': '<rootDir>/src/components$1',
    '^@composed-components(.*)$': '<rootDir>/src/composed-components$1',
    '^@pages(.*)$': '<rootDir>/src/pages$1',
    '^@reducers(.*)$': '<rootDir>/src/reducers$1',
  },
  rootDir: '.',
  moduleFileExtensions: ['js', 'json',],
  testRegex: '.*\\.test\\.js$',
  transformIgnorePatterns: ['node_modules'],
  testEnvironment: 'jest-environment-jsdom',
  transform: {
    "^.+\\.js$": "babel-jest"
  },
};
