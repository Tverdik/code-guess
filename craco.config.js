const path = require('path');

module.exports = {
  webpack: {
    alias: {
      '@components': path.resolve(__dirname, 'src/components'),
      '@composed-components': path.resolve(__dirname, 'src/composed-components'),
      '@providers': path.resolve(__dirname, 'src/providers'),
      '@reducers': path.resolve(__dirname, 'src/reducers'),
    },
  },
};
