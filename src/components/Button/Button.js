import React from 'react';
import styled from 'styled-components';

const StyledButton = styled.button`
  width: 100px;
  height: 35px;
  border-radius: 10px;
  border: 1px solid black;
  color: white;
  background: black;
  font-size: 16px;
  font-weight: bold;
  cursor: pointer;
  &:hover {
    transition: all ease 0.3s;
    background: white;
    color: black;
  }
`;

const Button = ({ children, onClick }) => (
  <StyledButton onClick={onClick} aria-label={'code-button'} >
    {children}
  </StyledButton>
);

export default Button;
