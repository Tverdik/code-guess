import React from 'react';
import { screen, render, fireEvent } from '@testing-library/react';
import QuizInput from './QuizInput';

describe('tests for QuizInput', () => {
  it('can have only 1 character', () => {
    render(
      <QuizInput />
    );

    const input = screen.getByLabelText('code-input');

    fireEvent.change(input, { target: { value: '12' }});

    expect(input.value).toBe('2');
  });

  it('can recieve disabled by prop', () => {
    render(
      <QuizInput disabled={true} />
    );

    const input = screen.getByLabelText('code-input');

    expect(input.disabled).toBe(true);
  });

  it('can recieve onChange by props', () => {
    const changeHandler = jest.fn();

    render(
      <QuizInput onChange={changeHandler} />
    );

    const input = screen.getByLabelText('code-input');
    fireEvent.change(input, { target: { value: '1' }});

    expect(changeHandler).toBeCalledTimes(1);
  });

  it('should pass id prop as a second argument in onChange function', () => {
    const changeHandler = jest.fn();

    render(
      <QuizInput id={0} onChange={changeHandler} />
    );

    const input = screen.getByLabelText('code-input');
    fireEvent.change(input, { target: { value: '3' }});

    expect(changeHandler).toHaveBeenCalledWith({ value: '3', id: 0 });
  });

  it('should handle only number values', () => {
    render(
      <QuizInput />
    );

    const input = screen.getByLabelText('code-input');
    fireEvent.change(input, { target: { value: 'aa' }});

    expect(input.value).toBe('');
  });

  it('should handle only positive numbers', () => {
    render(
      <QuizInput />
    );

    const input = screen.getByLabelText('code-input');
    fireEvent.change(input, { target: { value: '-3' }});

    expect(input.value).toBe('3');
  });
});
