import React from 'react';
import styled from 'styled-components';

const StyledInput = styled.input`
  width: 100px;
  height: 30px;
  border-radius: 10px;
  border: 1px solid black;
  font-size: 16px;
  font-weight: bold;
  padding: 0 10px;
  margin-bottom: 10px;
  text-align: center;
`;

const QuizInput = ({ onChange = () => {}, disabled = false, name, id }) => (
  <StyledInput
    type={'number'}
    onChange={({ target }) => {
      if (target.value.length > 1) {
        target.value = target.value[target.value.length - 1];
      }

      onChange({ value: target.value, id });
    }}
    disabled={disabled}
    name={name}
    aria-label={'code-input'} />
);

export default React.memo(QuizInput);
