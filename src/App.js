import { QuizProvider } from './providers/quiz.provider';
import QuizPage from './pages/QuizPage.page';
import './App.css';

const App = () => {
  return (
    <QuizProvider>
      <QuizPage />
    </QuizProvider>
  );
}

export default App;
