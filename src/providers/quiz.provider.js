import React, { useMemo } from 'react';
import { useContext, createContext, useReducer } from 'react';
import { quizReducers } from '@reducers/quiz.reducer';

const QuizContext = createContext({});

export const QuizProvider = ({ children }) => {
  const [state, dispatch] = useReducer((state, action) => {
    return quizReducers[action.type](state, action);
  }, {
    inputStates: [],
    stage: 0,
  });

  const quizMethods = useMemo(() => ({
    setCodeLength: codeLength => dispatch({ type: 'setCodeLength', payload: codeLength }),
    setOriginCodeItem: payload => dispatch({ type: 'setOriginCodeItem', payload }),
    setInputGuess: id => dispatch({ type: 'setInputGuess', payload: id }),
    setStage: stage => dispatch({ type: 'setStage', payload: stage }),
  }), []);

  return (
    <QuizContext.Provider value={{ state, quizMethods }}>
      {children}
    </QuizContext.Provider>
  );
};

export const useQuiz = () => useContext(QuizContext);
