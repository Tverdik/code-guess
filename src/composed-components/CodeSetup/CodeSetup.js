import React from 'react';
import QuizInput from '@components/QuizInput/QuizInput';
import Button from '@components/Button/Button';

const CodeSetup = ({ inputStates, setOriginCodeItem, setStage }) => (
  <div className="flex">
    <div>
      { inputStates.map(({ id }) => <QuizInput key={id} id={id} onChange={setOriginCodeItem} />) }
    </div>

    <Button onClick={setStage}>
      Start
    </Button>
  </div>
);

export default CodeSetup;
