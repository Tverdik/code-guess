import React from 'react';
import { screen, render, fireEvent } from '@testing-library/react';
import Game from './Game';

const noOneGuessed = [
  { value: '1', guessed: false, current: true, id: 0 },
  { value: '4', guessed: false, current: false, id: 1 },
  { value: '6', guessed: false, current: false, id: 2 },
];
const withGuessed = [
  { value: '1', guessed: true, current: false, id: 0 },
  { value: '4', guessed: false, current: true, id: 1 },
  { value: '6', guessed: false, current: false, id: 2 },
];

describe('tests for game', () => {
  it('only current input should be enabled', () => {
    render(
      <Game inputStates={noOneGuessed} setInputGuess={() => {}} />
    );

    const inputs = screen.getAllByLabelText('code-input');

    expect(inputs[0].disabled).toBe(false);
    expect(inputs[1].disabled).toBe(true);
    expect(inputs[2].disabled).toBe(true);
  });

  it('should render checkmark if input value guessed', () => {
    render(
      <Game inputStates={withGuessed} setInputGuess={() => {}} />
    );

    const inputs = screen.getAllByLabelText('code-input');
    const checkmarks = screen.getAllByLabelText('checkmark');

    expect(inputs.length).toBe(2);
    expect(checkmarks.length).toBe(1);
    expect(inputs[0].disabled).toBe(false);
    expect(inputs[1].disabled).toBe(true);
  });

  it(`shouldn't call setInputGuess if input value is wrong`, () => {
    const setInputGuess = jest.fn();

    render(
      <Game inputStates={noOneGuessed} setInputGuess={setInputGuess} />
    );

    const [input] = screen.getAllByLabelText('code-input');

    fireEvent.change(input, { target: { value: '3' }});

    expect(setInputGuess).toHaveBeenCalledTimes(0);
  });

  it(`should call setInputGuess if input value is correct`, () => {
    const setInputGuess = jest.fn();

    render(
      <Game inputStates={noOneGuessed} setInputGuess={setInputGuess} />
    );

    const [input] = screen.getAllByLabelText('code-input');

    fireEvent.change(input, { target: { value: '1' }});

    expect(setInputGuess).toHaveBeenCalledTimes(1);
  });
});
