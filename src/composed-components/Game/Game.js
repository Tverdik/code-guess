import React from 'react';
import styled from 'styled-components';
import { CheckSquare } from '@styled-icons/bootstrap/CheckSquare';
import QuizInput from '@components/QuizInput/QuizInput';

const CheckSquareStyled = styled(CheckSquare)`
  height: 25px;
  width: 122px;
  margin-bottom: 10px;
`;

const Game = ({ inputStates, setInputGuess }) => (
  <div>
    {
      inputStates.map(({ value, id, current, guessed }) => (
        guessed ? <CheckSquareStyled aria-label="checkmark" key={id} /> : <QuizInput
          key={id}
          disabled={!current}
          onChange={({ value: targetValue }) => {
            if (targetValue !== value) {
              return;
            }
            setInputGuess(id);
          }} />
      ))
    }
  </div>
);

export default Game;
