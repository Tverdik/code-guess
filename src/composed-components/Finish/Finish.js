import React from 'react';
import Button from '@components/Button/Button';

const Finish = ({ onFinish }) => (
  <div className="flex">
    <div className="notification">
      <h4>Congratulations!</h4>
      <p>You guessed the code!</p>
    </div>

    <Button onClick={onFinish}>
      Restart
    </Button>
  </div>
);

export default Finish;
