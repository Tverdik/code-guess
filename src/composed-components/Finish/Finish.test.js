import React from 'react';
import { render } from '@testing-library/react';
import Finish from './Finish';

describe('tests for finish', () => {
  it('should render correctly', () => {
    const { container } = render(
      <Finish />
    );

    expect(container).toMatchSnapshot();
  });
})