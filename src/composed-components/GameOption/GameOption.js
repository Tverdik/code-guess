import React from 'react';
import Button from '@components/Button/Button';
import QuizInput from '@components/QuizInput/QuizInput';

const GameOption = ({ setCodeLength }) => (
  <div className="flex">
    <h5 style={{ marginBottom: '10px' }}>Specify inputs amount (up to 9):</h5>
    <form aria-label="code-form" type="submit" onSubmit={(e) => {
        e.preventDefault();

        if (Number(e.target.inputAmount.value) <= 0) {
          return;
        }

        setCodeLength(Number(e.target.inputAmount.value));
      }}>
      <QuizInput name="inputAmount" />
      <Button>Select</Button>
    </form>
  </div>
);

export default GameOption;
