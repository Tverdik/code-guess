import { quizReducers } from './quiz.reducer';

const reducer = (state, action) => {
  if (!quizReducers[action.type]) {
    return;
  }

  return quizReducers[action.type](state, action);
};

const initialValue = {
  inputStates: [
    { id: 0, value: '', guessed: false, current: true },
    { id: 1, value: '', guessed: false,  current: false }
  ],
  stage: 1
};

describe('tests for quiz reducer', () => {
  describe('tests for setCodeLength', () => {
    it('should create states for inputs', () => {
      const action = {
        type: 'setCodeLength',
        payload: 2,
      };
      const newState = reducer({}, action);

      expect(newState).toEqual(initialValue);
    });

    it('should change stage to 1', () => {
      const action = {
        type: 'setCodeLength',
        payload: 2,
      };
      const newState = reducer({}, action);

      expect(newState.stage).toBe(1);
    });
  });

  describe('tests for setOriginCodeItem', () => {
    it('should change specified input value', () => {
      const action = {
        type: 'setOriginCodeItem',
        payload: { value: '3', id: 0 },
      };
      const newState = reducer(initialValue, action);

      expect(newState.inputStates[0].value).toBe('3');
    });
  });

  describe('tests for setInputGuess', () => {
    it('should change inputState guessed to true', () => {
      const action = {
        type: 'setInputGuess',
        payload: 0,
      };
      const state = reducer(initialValue, action);

      expect(state.inputStates[0].guessed).toBe(true);
    });

    it(`should change inputState current to false and next input's to true`, () => {
      const action = {
        type: 'setInputGuess',
        payload: 0,
      };
      const state = reducer(initialValue, action);

      expect(state.inputStates[0].guessed).toBe(true);
      expect(state.inputStates[0].current).toBe(false);
      expect(state.inputStates[1].current).toBe(true);
    });

    it('should change stage to 3 if last input was guessed', () => {
      const firstAction = {
        type: 'setInputGuess',
        payload: 0,
      };
      const secondAction = {
        type: 'setInputGuess',
        payload: 1,
      };
      const firstGuessed = reducer(initialValue, firstAction);
      const state = reducer(firstGuessed, secondAction);

      expect(state.inputStates[0].guessed).toBe(true);
      expect(state.inputStates[1].guessed).toBe(true);
      expect(state.stage).toBe(3);
    });
  });

  describe('tests for setStage', () => {
    it('should change stage to specified', () => {
      const action = {
        type: 'setStage',
        payload: 3,
      };
      const state = reducer(initialValue, action);

      expect(state.stage).toBe(3);
    });
  });
});
