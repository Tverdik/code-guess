export const quizReducers = {
  'setCodeLength': (state, { payload: codeLength }) => {
    const newInputStates = new Array(codeLength)
      .fill(null)
      .map((_, idx) => ({
        id: idx,
        value: '',
        guessed: false,
        current: idx === 0,
      }));

    return { ...state, inputStates: newInputStates, stage: 1 };
  },
  'setOriginCodeItem': (state, { payload: { value, id }}) => {
    const newInputStates = [...state.inputStates];

    newInputStates[id].value = value;

    return { ...state, inputStates: newInputStates };
  },
  'setInputGuess': (state, { payload: id }) => {
    const newInputStates = [...state.inputStates];

    newInputStates[id].guessed = true;
    newInputStates[id].current = false;

    if (id !== newInputStates.length - 1) {
      newInputStates[id + 1].current = true;
    } else {
      return { ...state, stage: 3 };
    }

    return { ...state };
  },
  'setStage': (state, { payload: stage }) => ({ ...state, stage })
};
