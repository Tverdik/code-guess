import React, { useCallback } from 'react';
import { useQuiz } from '@providers/quiz.provider';
import GameOption from '@composed-components/GameOption/GameOption';
import CodeSetup from '@composed-components/CodeSetup/CodeSetup';
import Game from '@composed-components/Game/Game';
import Finish from '@composed-components/Finish/Finish'; 

const QuizPage = () => {
  const { 
    state: { inputStates, stage },
    quizMethods: { setCodeLength, setOriginCodeItem, setInputGuess, setStage }
  } = useQuiz();

  return (
    <div className="flex wrapper">
      { stage === 0 && <GameOption setCodeLength={setCodeLength} /> }
      { 
        stage === 1 && <CodeSetup
          inputStates={inputStates}
          setOriginCodeItem={setOriginCodeItem}
          setStage={() => setStage(2)} />
      }
      { (stage === 2 || stage === 3) && <Game inputStates={inputStates} setInputGuess={setInputGuess} /> }
      { stage === 3 && <Finish onFinish={() => setStage(0)} />}
    </div>
  )
}

export default QuizPage;
